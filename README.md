# SPSP Public docs

## Built with

- [VitePress](https://vitepress.dev/)

## Content

To edit the content, edit the markdown files in the `docs` folder.

## Customization

To customize the navbar or the sidebar, edit the `docs/.vitepress/config.mts` file.

To customize the welcome page, edit the `docs/index.md` file.

## Development

Install dependencies:

```bash
# Install dependencies
npm install
```

Start the development server:

```bash
# Start the development server
npm run docs:dev
```

Build the static site:

```bash
# Build the static site
npm run docs:build
```

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Maintainers

- [Dillenn Terumalai](mailto:dillenn.terumalai@sib.swiss)
