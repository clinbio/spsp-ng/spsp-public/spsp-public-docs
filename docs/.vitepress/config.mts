import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "SPSP",
  description: "Documentation",
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    search: {
      provider: 'local',
    },

    nav: [
      { text: 'Public portal', link: 'https://public.spsp.sib.swiss' },
      { text: 'SPSP GitLab', link: 'https://gitlab.sib.swiss/clinbio/spsp-ng/spsp-public' },
      { text: 'spsp.ch', link: 'https://spsp.ch' },
    ],

    sidebar: [
      {
        text: 'Public portal',
        items: [
          { text: 'Public API', link: '/public-portal#public-api' },
        ]
      },
      {
        text: 'Secure Private portal',
        items: [
          { text: 'User registration', link: '/secure-private-portal#user-registration' },
          { text: 'User documentation', link: '/secure-private-portal#user-documentation' }
        ]
      },
      {
        text: 'Instructions for new groups',
        items: [
          { text: 'Set up the SPSP transfer tool', link: '/instructions-for-new-registered-groups#set-up-the-spsp-transfer-tool' },
          { text: 'Get the metadata template file', link: '/instructions-for-new-registered-groups#get-the-metadata-template-file' },
          { text: 'Share your ENA center name for data exports', link: '/instructions-for-new-registered-groups#share-your-ena-center-name-for-data-exports' },
          { text: 'Summary of data processing upon submission', link: '/instructions-for-new-registered-groups#summary-of-data-processing-upon-submission' }
        ]
      },
      {
        text: 'Data submission',
        items: [
          { text: 'Data types', link: '/data-submission#data-types' },
          { text: 'Prepare the data', link: '/data-submission#prepare-the-data-using-the-metadata-template-file-corresponding-to-your-data-type' },
          { text: 'Submit the data', link: '/data-submission#submit-the-data-using-the-transfer-tool' },
        ]
      },
      {
        text: 'Data access and re-use',
        items: [
          { text: 'Data access rules', link: '/data-access-and-re-use#data-access-rules' },
        ]
      },
      {
        text: 'Contact',
        items: [
          { text: 'SPSP Support team', link: '/data-access-and-re-use#contact' },
        ]
      },
    ],
  },
  base: '/docs/'
})
