---
outline: deep
prev:
    text: 'Data submission'
    link: '/data-submission'
next: false
---

# Data access and re-use

Data access and re-use are described in SPSP legal documents (SPSP Consortium Agreement and SPSP Data Transfer and Use Agreement).

## Data access rules

1. Each type of data is subject to specific access rules, as defined below under 1.2. The SPSP Platform is accessible only to the following users (the “Registered Users”):

    * SPSP REGISTERED GROUPS;
    * The THIRD PARTIES: institutions or person not member of the CONSORTIUM AGREEMENT and not providing data to the SPSP Platform whose access to the SPSP Platform have been duly validated by the SPSP EXECUTIVE BOARD, either for surveillance purposes or for a specific research project validated by the competent ethics committee.

2. Depending on the type of data accessed, four levels of access to the SPSP Platform are granted to Registered Users:

    * **No Access**: no data is accessible to non-Registered Users of SPSP, unless they are already publicly available on international repositories; in that case, minimal de-identified data are available through the [SPSP Public portal](https://public.spsp.sib.swiss).
    * **Restricted Access (R)**: by default, Registered Users only have access to minimal data on the cases submitted by other Registered Users, precluding patient-identification;
    * **Extended Access (E)**: upon approval of the SPSP EXECUTIVE BOARD, additional data (including personal data) may be made accessible to Registered User belonging to another Registered User, if required for a specific study research question and if authorised by the competent ethics committee;
    * **Full Access (F)**: Registered Users belonging to the same laboratory have access to the whole DATA submitted by any member of their laboratory, including personal data.

## Contact

For any question or suggestion on SPSP, please send an email to [SPSP support](mailto:spsp-support@sib.swiss).