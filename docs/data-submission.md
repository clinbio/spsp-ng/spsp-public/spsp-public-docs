---
outline: deep
prev:
    text: 'Instructions for new groups and users'
    link: '/instructions-for-new-groups-and-users'
next:
    text: 'Data access and re-use'
    link: '/data-access-and-re-use'
---

# Instructions for data submission
Version 1 - August 2024

## Data types
SPSP accepts submissions for the following data types:
- Generic bacteria ("SPSP-Bacteria_Metadata_submission_v*"). The submitted data are processed with the [IMMense pipeline](https://gitlab.sib.swiss/clinbio/spsp-ng/spsp-ng-bioinformatics/pipelines/immense). Minimal data are shared with the ENA.
- Bacteria for the SPHN NDS IICU project ("SPSP-IICU_Metadata_submission_v*"). The submitted data are processed with the [IMMense pipeline](https://gitlab.sib.swiss/clinbio/spsp-ng/spsp-ng-bioinformatics/pipelines/immense). Minimal data are shared with the ENA.
- Generic viruses ("SPSP-Virus_Metadata_submission_v*"). Minimal data are shared with the ENA (no bioinformatics pipeline).
- SARS-CoV-2 ("SPSP-SCV2_Metadata_submission_v*"). The submitted data are annotated witih [pangolin](https://cov-lineages.org/resources/pangolin.html) and [nextclade](https://clades.nextstrain.org/). Minimal data are shared with the ENA and GISAID.
- Influenza/RSV ("SPSP-InfluenzaRSV_Metadata_submission_v*"). The submitted data are processed with [IRMA](https://wonder.cdc.gov/amd/flu/irma/), [FluR](https://gitlab.sib.swiss/clinbio/spsp-ng/spsp-ng-bioinformatics/pipelines/influenza-resistance) (resistance mutations) and displayed using [auspice](https://auspice.spsp.sib.swiss/) (nextstrain frontend). Minimal data are shared with the ENA and GISAID.

## Prepare the data using the metadata template file corresponding to your data type
Please use the data-specific SPSP metadata template file to describe the data submitted to SPSP. You can use either the Excel (.xlsx) template or a simple TSV (.tsv) using the variable names as header. In any case, please make sure to carefully read the explanations and examples provided for each field in the Excel template prior to starting. 
[The latest version of the template and instructions is available here](https://sibcloud-my.sharepoint.com/:f:/g/personal/aitana_neves_sib_swiss/EtEXv2OW6YtHsGqL232kSyoBIYDTbqyOnJg1AnogL5VlEQ?e=6a3AzM ). 
In case of questions, please contact [SPSP support](mailto:spsp-support@sib.swiss).

## Submit the data using the transfer tool
* If you have not yet set up the tool Sendcrypt, please refer to [Instruction for new groups](https://public.spsp.sib.swiss/docs/instructions-for-new-groups.html#set-up-the-spsp-transfer-tool).
* Please refer to the [Sendcrypt documentation](https://clinbiokb.sib.swiss/s/sendcrypt) for submitting your data using either the graphical user interface, or the command line tool. Every batch consists of a metadata file and corresponding data (e.g. raw genomic data).

## Contact
For any question or suggestion on SPSP, please send an email to [SPSP support](mailto:spsp-support@sib.swiss).
