---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Swiss Pathogen Surveillance Platform"
  text: "Documentation"
  tagline: "A secure One-health online platform that enables near real-time sharing under controlled access of pathogen sequencing data and their associated clinical/epidemiological metadata."
  image:
    src: /logo.png
    alt: VitePress
  actions:
    - theme: brand
      text: Get Started
      link: /public-portal
    - theme: alt
      text: SPSP GitLab
      link: https://gitlab.sib.swiss/clinbio/spsp-ng/spsp-public
    - theme: alt
      text: spsp.ch
      link: https://spsp.ch


features:
  - icon: 🌐
    title: Public API
    details: User documentation for browsing the available data on SPSP
    link: /public-portal#public-api
  - icon: 🔐
    title: Secure private portal
    details: User documentation for logging and browsing data on the secure private SPSP (registered groups and users only)
    link: /secure-private-portal
  - icon: 📖
    title: Instructions for new groups
    details: Everything you need to know once your group has joined SPSP.
    link: /instructions-for-new-groups
  - icon: 📊
    title: Data submissions
    details: Procedure for submitting data to SPSP
    link: /data-submission
  - icon: 📊
    title: Data access and re-use
    details: Procedure for requesting access to data for a research project
    link: /data-access-and-re-use
---

