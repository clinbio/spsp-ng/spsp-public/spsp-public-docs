---
outline: deep
prev: false
next:
  text: 'Secure private portal'
  link: '/secure-private-portal'
---

# Public portal

## Public API

The public API provides access to the public data stored in the Swiss Pathogen Surveillance Platform (SPSP). The data is organized in datasets, each containing different categories of samples. The public API allows users to browse the available data on SPSP.

## Authentication

The public API does not require any authentication.

## Endpoints

### Base URL

The base URL of the public API is 

`https://public.spsp.sib.swiss/api/v1`

The public API provides the following endpoints:

- [`/datasets`](#get-datasets): list all datasets
- [`/{dataset}/{category}/last_update`](#get-dataset-category-last-update): get the last update of a category in a dataset
- [`/{dataset}/{category}/fields`](#get-dataset-category-fields): list all fields of a given category in a dataset
- [`/{dataset}/{category}/samples`](#get-dataset-category-samples): paginated list all samples of a given category in a dataset
- [`/{dataset}/{category}/submissions`](#get-dataset-category-submissions): list all submissions by week of a given category in a dataset
- [`/{dataset}/{category}/stats`](#get-dataset-category-stats): get statistics of a given category in a dataset

### **GET** `/datasets`

#### Description

Get the list of all datasets with their categories.

#### Try it out

[https://public.spsp.sib.swiss/api/v1/datasets](https://public.spsp.sib.swiss/api/v1/datasets)

#### cURL

```bash
curl -X GET "https://public.spsp.sib.swiss/api/v1/datasets" -H "accept: application/json"
```

#### Example response

```json
{
  "viruses": [
    "sars-cov-2",
    "influenza",
    "other"
  ],
  "wastewater": [
    "sars-cov-2"
  ]
}
```

### **GET** `/{dataset}/{category}/last_update`

#### Description

Get the last update of a category in a dataset.

#### Try it out

[https://public.spsp.sib.swiss/api/v1/viruses/sars-cov-2/last_update](https://public.spsp.sib.swiss/api/v1/viruses/sars-cov-2/last_update)

#### cURL

```bash
curl -X GET "https://public.spsp.sib.swiss/api/v1/viruses/sars-cov-2/last_update" -H "accept: application/json"
```

#### Example response

```json
{
  "last_update": "2024-10-09"
}
```

### **GET** `/{dataset}/{category}/fields`

#### Description

Get the list of all the available fields of a given category in a dataset.

#### Try it out

[https://public.spsp.sib.swiss/api/v1/viruses/sars-cov-2/fields](https://public.spsp.sib.swiss/api/v1/viruses/sars-cov-2/fields)

#### cURL

```bash
curl -X GET "https://public.spsp.sib.swiss/api/v1/viruses/sars-cov-2/fields" -H "accept: application/json"
```

#### Example response

```json
[
  {
    "id": 1,
    "name": "ISOLATE.SPSP_identifier",
    "is_public": true,
    "type": "STRING",
    "values": null
  },
  {
    ...
  },
  {
    "id": 74,
    "name": "FRAMESHIFTS.Frameshift_diagnosis",
    "is_public": false,
    "type": "BOOLEAN",
    "values": null
  }
]
```

### **GET** `/{dataset}/{category}/samples`

#### Description

Get the paginated list of all samples of a given category in a dataset.

#### Try it out

[https://public.spsp.sib.swiss/api/v1/viruses/sars-cov-2/samples?page=1&items-per-page=1&sort=-ISOLATE.SPSP_identifier](https://public.spsp.sib.swiss/api/v1/viruses/sars-cov-2/samples?page=1&items-per-page=1&=-ISOLATE.SPSP_identifier)

#### cURL

```bash
curl -X GET "https://public.spsp.sib.swiss/api/v1/viruses/sars-cov-2/samples?page=1&items-per-page=1&sort=-ISOLATE.SPSP_identifier" -H "accept: application/json"
```

#### Query parameters

- `page` (integer): page number (required)
- `items-per-page` (integer): number of samples per page (required)
- `{field}` (string): filter samples by field value (optional)
- `sort` (string): sort samples by field (optional) (use `-` to sort in descending order)

#### Example response

```json
{
  "current_page": 1,
  "last_page": 164600,
  "from": 1,
  "to": 1,
  "total": 164600,
  "data": [
    {
      "ISOLATE.SPSP_identifier": "SPSP155732",
      "ISOLATE.Strain_name": "hCoV-19/Switzerland/AG-UZH-1000477914/2020",
      "ISOLATE.Species_taxonomy_ID": 2697049,
      "ISOLATE.Species_name": "SARS-CoV-2",
      "ISOLATE.Genus": "Betacoronavirus",
      "ISOLATE.Isolation_date": "2020-03-01",
      "ISOLATE.Isolation_compartment": "HUMAN",
      "ISOLATE.Isolation_site": false,
      "ISOLATE.Isolation_location_-_continent": "EUROPE",
      "ISOLATE.Isolation_location_-_country": "SWITZERLAND",
      "ISOLATE.Isolation_location_-_division": "AG",
      "ISOLATE.Isolation_location_-_ZIP": false,
      "ISOLATE.Isolation_location_-_address": false,
      "ISOLATE.Exposure_location_-_continent": true,
      "ISOLATE.Exposure_location_-_country": true,
      "ISOLATE.Exposure_location_-_division": true,
      "ISOLATE.Reporting_lab_order_id": false,
      "ISOLATE.Collecting_lab_order_id": false,
      "ISOLATE.Collecting_lab_code": false,
      "ISOLATE.Collecting_lab_name": "Department of Infectious Diseases, Cantonal Hospital Baden",
      "HOST.Host_age": true,
      "HOST.Host_sex": true,
      "HOST.Host_clinical_status": false,
      "SUBMISSION.Batch_name": false,
      "SUBMISSION.Batch_submission_date": true,
      "SUBMISSION.Reporting_lab_name": "Institute of Medical Virology, University of Zurich",
      "SEQUENCING.Sequencing_lab_name": null,
      "SEQUENCING.Library_preparation_kit": null,
      "SEQUENCING.Sequencing_platform": null,
      "SEQUENCING.General_sequencing_purpose": "OTHER",
      "SEQUENCING.Detailed_sequencing_purpose": false,
      "RAW_DATASET.Raw_data_coverage": null,
      "RAW_DATASET.Raw_data_embargo_date": null,
      "RAW_DATASET.Raw_data": false,
      "ASSEMBLY.Assembly_method": null,
      "ASSEMBLY.Assembly_length": 29862,
      "ANNOTATION_PANGOLIN.Pangolin_lineage": "B.1.1",
      "ANNOTATION_PANGOLIN.Pangolin_scorpio_lineage": "",
      "ANNOTATION_PANGOLIN.Pangolin_version": "4.3.1",
      "ANNOTATION_NEXTCLADE.Nextclade_clade": "20B",
      "ANNOTATION_NEXTCLADE.Nextclade_pangolin_lineage": "B.1.1",
      "ANNOTATION_NEXTCLADE.Substitutions": "C241T,C3037T,C14408T,A23403G,G28881A,G28882A,G28883C,T29148C",
      "ANNOTATION_NEXTCLADE.Deletions": "",
      "ANNOTATION_NEXTCLADE.Insertions": "",
      "ANNOTATION_NEXTCLADE.Amino_acid_substitutions": "N:R203K,N:G204R,N:I292T,ORF1b:P314L,S:D614G",
      "ANNOTATION_NEXTCLADE.Amino_acid_deletions": "",
      "ANNOTATION_NEXTCLADE.Amino_acid_insertions": "",
      "ANNOTATION_NEXTCLADE.Frameshifts": "",
      "ANNOTATION_NEXTCLADE.Missing": "",
      "ANNOTATION_NEXTCLADE.Total_missing": 0,
      "ANNOTATION_NEXTCLADE.Non_ACGTNs": "",
      "ANNOTATION_NEXTCLADE.Total_non_ACGTNs": 0,
      "ANNOTATION_NEXTCLADE.Private_mutations_-_Labeled_substitutions": "",
      "ANNOTATION_NEXTCLADE.Private_mutations_-_Reversions": "",
      "ANNOTATION_NEXTCLADE.Private_mutations_-_Unlabeled_substitutions": "T29148C",
      "ANNOTATION_NEXTCLADE.Private_mutations_-_Total_labeled": 0,
      "ANNOTATION_NEXTCLADE.Private_mutations_-_Total_reversions": 0,
      "ANNOTATION_NEXTCLADE.Private_mutations_-_Total_unlabeled": 1,
      "ANNOTATION_NEXTCLADE.Private_mutations_-_Total_private": 1,
      "ANNOTATION_NEXTCLADE.Private_mutations_-_QC_total": 1,
      "ANNOTATION_NEXTCLADE.Private_mutations_-_QC_status": "good",
      "ANNOTATION_NEXTCLADE.Total_PCR_primer_changes": 0,
      "ANNOTATION_NEXTCLADE.Nextclade_QC_score": 0,
      "ANNOTATION_NEXTCLADE.Nextclade_QC_status": "good",
      "ANNOTATION_NEXTCLADE.Nextclade_version": "3.8.2",
      "EXTERNAL_IDENTIFIER.GISAID_identifier": "EPI_ISL_524474",
      "EXTERNAL_IDENTIFIER.ENA_sample_identifier": "ERS6146590",
      "EXTERNAL_IDENTIFIER.ENA_project_identifier": "PRJEB43828",
      "EXTERNAL_IDENTIFIER.ENA_analysis_identifier": "ERZ1765023",
      "EXTERNAL_IDENTIFIER.ENA_sequence_id_identifier": "FR990296.1",
      "EXTERNAL_IDENTIFIER.ENA_experiment_identifier": null,
      "EXTERNAL_IDENTIFIER.ENA_run_identifier": null,
      "FRAMESHIFTS.Frameshift_position": false,
      "FRAMESHIFTS.Frameshift_diagnosis": false
    }
  ]
}
```

### **GET** `/{dataset}/{category}/submissions`

#### Description

Get the list of all submissions of a given category in a dataset by week.

#### Try it out

[https://public.spsp.sib.swiss/api/v1/viruses/sars-cov-2/submissions](https://public.spsp.sib.swiss/api/v1/viruses/sars-cov-2/submissions)

#### cURL

```bash
curl -X GET "https://public.spsp.sib.swiss/api/v1/viruses/sars-cov-2/submissions" -H "accept: application/json"
```

#### Example response

```json
[
  {
    "submissions_nb": 3154,
    "week": "2021-03-29"
  },
  {
    ...
  },
  {
    "submissions_nb": 46,
    "week": "2024-08-26"
  }
]
```

### **GET** `/{dataset}/{category}/stats`

#### Description

Get the statistics of a given category of a dataset.

#### Try it out

[https://public.spsp.sib.swiss/api/v1/viruses/sars-cov-2/stats](https://public.spsp.sib.swiss/api/v1/viruses/sars-cov-2/stats)

#### cURL

```bash
curl -X GET "https://public.spsp.sib.swiss/api/v1/viruses/sars-cov-2/stats" -H "accept: application/json"
```

#### Example response

```json
{
  "species": "all",
  "sample_nb": 164600,
  "raw_datasets_nb": 141449,
  "assemblies_nb": 164600,
  "last_submission_date": "2024-08-29"
}
```

## Contact

For any question or suggestion on SPSP, please send an email to [SPSP support](mailto:spsp-support@sib.swiss).