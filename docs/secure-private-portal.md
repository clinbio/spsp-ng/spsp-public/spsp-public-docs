---
outline: deep
prev:
    text: 'Public portal'
    link: '/public-portal'
next:
    text: 'Instructions for new groups and users'
    link: '/instructions-for-new-groups-and-users'
---

# Secure private portal

## User registration

Access to the SPSP Frontend is only possible with a registered user account. 
In order to request a SPSP user account, your group needs to be previously registered within SPSP. Please contact [SPSP support](mailto:spsp-support@sib.swiss) if your group is not yet registered.
We follow SPHN recommendations for user authentication on the SPSP Frontend:
-	Personal digital identity relies on a SWITCH edu-ID account.
-	Authentication is managed by Keycloack, an open source identity and access management solution. SPSP relies on a Keycloack instance set up by the SPHN Data Coordination Center for the SIB Clinical Bioinformatics group, which is responsible for SPSP development and maintenance.
-	Two-factor authentication is enforced at login.
-	The login page is only accessible to previously white-listed IP addresses.

### Instructions to request a new user account

1.	On a web browser, please visit: <https://spsp.sib.swiss>. 
> :bulb: **Tip:** Please make sure to connect from your institution or using your institution’s vpn as the link will only work when connecting from previously white-listed IPs.

2.	Click on “SWITCH edu-ID IdP” to be redirected to the SWITCH edu-ID page.
![user-doc-1](/user-doc-1.png)

3.	On the SWITCH page, enter your email/ password for your edu-ID account or create a new edu-ID account if you do not have one yet.
> :bulb: **Tip:** Please make sure to active two-factor authentication on your SWITCH edu-ID account, else you will not be able to access the SPSP frontend.

![user-doc-2](/user-doc-2.png)

4.	After you login with your edu-ID account, you will need to fill in a short form.
> :bulb: **Tip:** This form will be used to validate that you belong to a SPSP registered group and to contact your group head to ask for your role on the SPSP Frontend (viewer or editor). 

![user-doc-3](/user-doc-3.png)

5.	Once we receive all the required information from your group head, SPSP support will contact you by email to inform you that your account has been activated. 
> :bulb: **Tip:** Please refer to [User documentation section](#user-documentation) for accessing and using the SPSP Frontend. 

## User documentation

### General view of your lab

This is the landing page after you login. From here, you can see the activity of your laboratory, view the existing projects and add a new project.

![user-doc-4](/user-doc-4.png)

### Create new project / Edit project
Projects are created for the laboratory and will be visible by all the group members.
Give a title and a description to your project.

![user-doc-5](/user-doc-5.png)

Every project is based on a query. As new samples are submitted to SPSP, they will be automatically added to your project if they match its associated query.

By default, you only have access to a limited set of fields on the samples submitted by others. Hence, if you select restricted fields for your query, only those samples for which you have extended access will show up in the resulting table. 

![user-doc-6](/user-doc-6.png)

![user-doc-7](/user-doc-7.png)

Once you defined your query, you can select which columns should be displayed in the resulting table. Note that if you select restricted fields, you will only view the data for those samples for which you have extended access.

![user-doc-8](/user-doc-8.png)
![user-doc-9](/user-doc-9.png)

Once you are done, you can save your project.
![user-doc-10](/user-doc-10.png)

### Actions on the projects
Click on the My projects icon on the top navigation bar to have an overview of the projects belonging to your lab.

![user-doc-11](/user-doc-11.png)
![user-doc-12](/user-doc-12.png)

In the samples view, you see the table resulting from the query associated to the project. 

It is also possible to edit the project using the link on the left panel.

![user-doc-13](/user-doc-13.png)

### My uploads

This is where you can monitor all your submissions and see if there are any pending actions on your side.

The column “Update” indicates how many samples in the submission were actually an update of an existing sample.

When the matching consensus sequence or raw data is missing for a sample, it will appear in the “Waiting fasta” or “Waiting raw” columns, respectively.

![user-doc-14](/user-doc-14.png)

### My dashboard

Plots are automatically generated and organized into categories (tabs on the top of the page). Plots contain aggregated data from SPSP or laboratory-specific data. 

![user-doc-15](/user-doc-15.png)

![user-doc-16](/user-doc-16.png)

![user-doc-17](/user-doc-17.png)

![user-doc-18](/user-doc-18.png)

![user-doc-19](/user-doc-19.png)

![user-doc-20](/user-doc-20.png)

![user-doc-21](/user-doc-21.png)

![user-doc-22](/user-doc-22.png)

![user-doc-23](/user-doc-23.png)

![user-doc-24](/user-doc-24.png)

![user-doc-25](/user-doc-25.png)

![user-doc-26](/user-doc-26.png)

### Send us your feedback

If you encounter any bugs or would like to make suggestions, please us the “Send feedback” form. 

![user-doc-27](/user-doc-27.png)

## Contact
For any question or suggestion on SPSP, please send an email to [SPSP support](mailto:spsp-support@sib.swiss).